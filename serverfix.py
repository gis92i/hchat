import socket
import sys
from thread import *
from Crypto.Util.number import getRandomRange
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util.number import inverse
from Crypto.Util.number import long_to_bytes
from Crypto.Util.number import bytes_to_long
from Crypto.Hash import SHA256


class Server:
    """
    Server Class: g, p are given and shared accross all server instances
    The values of g and p were taken from the website https://www.ietf.org/rfc/rfc3526.txt
    """
    p = int("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF",16)
    g = 2



    def __init__(self,port):

        self.host = ''
        self.port = port
        self.conn = ''
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.a = getRandomRange(1,self.p)
        self.g_a = pow(self.g,self.a,self.p)
        self.diff = 0
        self.aes_key = ''
        self.iv = 16 * '\x00' #iv is constant so that we can encrypt and decrypt for the server and client wit the same iv
        self.commited_value = 0 #this value will be used for commitment in order to check the man in the middle

        print 'Socket created'
        try:
            self.s.bind((self.host,self.port))
        except socket.error, msg:
            print 'Bind failed. Error: ' + str(msg[0]) + 'Message ' + msg[1]
            sys.exit()
        print 'Socket bind complete'
        self.s.listen(1)
        print("Socket is now listening")


        """
        We start the secret channel talk for the man in the middle scheme right here
        """
        self.secret_channel_talk()

    def send_to_client(self,message):
        #Same thing as in the client side we encrypt then send
        #We encrypt wit AES and the key as gab
        cipher = AES.new(self.aes_key,AES.MODE_CFB, self.iv)
        msg = cipher.encrypt(message)
        self.conn.sendall(msg)

    def recv_from_client(self):
        #Same as on the client side we do the same thing
        msg = self.conn.recv(1024)
        #We decrypt using AES
        cipher = AES.new(self.aes_key,AES.MODE_CFB,self.iv)
        message = cipher.decrypt(msg)
        return message

    def handshake(self):
        #We will create an aes key here with the password being gab
        self.conn.sendall("Starting Authentication:")
        self.conn.sendall(str(self.g_a))
        g_b = int(self.conn.recv(1024))

        """
        Before we continue, we verify the value we have received through the secret channel
        """

        fhash = SHA256.new()
        fhash.update(long_to_bytes(g_b))
        if(self.commitedvalue != fhash.hexdigest()):
            print("This Client is a fake Client! Closing all the connections")
            #self.quit()
            return False

        self.diff = pow(g_b,self.a,self.p)
        temp = long_to_bytes(self.diff) #We set the key here
        h = SHA256.new()
        h.digest_size = 32
        h.update(temp)
        self.aes_key = h.digest()
        print(self.diff)
        return True



    def clientthread(self,conn):
        #We start with the handhsake first
        x = self.handshake()
        # Send welcome message
        if(x != True):
            print("Aborting!!!")
            self.conn.close()
            
        self.send_to_client("Welcome to the server. Type something and hit enter\n") #send only takes string
        #infinite loop so that function do not terminate and thread do not end.
        while True:
	        #Receiving from client
            data = self.recv_from_client()
            print(data)
            reply = raw_input("")
            if not data:
                break

            self.send_to_client(reply)
 
        #came out of loop 
        conn.close()

    def secret_channel_talk(self):
        """
        This function will be used to simulate a secret channel aka phone call for the commitment
        """
        conn,addr = self.s.accept() #accepting the client connection
        h = SHA256.new()
        h.update(long_to_bytes(self.g_a))
        conn.sendall(h.hexdigest()) #Sending the commitment for the server
        self.commitedvalue = conn.recv(1024) #Now that we have received the commited value of the client we are good
        print("Commitment process done!")

    def start_connection(self):
        #now keep talking with the client
        while 1:
            #print("Why is not working here...")
            #wait to accept a connection - blocking call
            self.conn, addr = self.s.accept()
            print 'Connected with ' + addr[0] + ':' + str(addr[1])

            #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
            start_new_thread(self.clientthread ,(self.conn,))

    def quit(self):
        self.s.close()


if __name__ == '__main__':
    server = Server(8888)
    #server.secret_channel_talk()
    server.start_connection()
    server.quit()
