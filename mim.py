import socket
import sys
from thread import *
from server import Server
from client import Client
from Crypto.Util.number import getRandomRange
from Crypto.Hash import SHA256


#make a server instance for the man in the middle that accepts the connection of the original Client 
mim_server = Server(6666)

#make a client instance for the man in the middle that will connect to the orignal server
original_host = 'localhost'
original_port = 8888
mim_client = Client(original_host,original_port)

#Now we start the diffie with the client

print('Connecting the fake server with original client\n')
mim_server.s.listen(1)
print("mim server is listening\n")
mim_server.conn,addr = mim_server.s.accept()
print 'Connected to Client: ' + addr[0] + ':' + str(addr[1])

print('Connecting the fake cleint to the original server\n')
mim_client.connect()
print('Fake client connected to original server\n')

#Now we also do the diffie with the server

print('starting handshakes\n')

print('Fake client handshake with the server **************\n')
x = mim_client.handshake()
print(mim_client.recv_from_server())
if(x == True):
	print('Fake Client handshake complete\n')
else:
	print("Fake Client handshake failed!\n")


print('Fake server handshake with client ***********\n')
y = mim_server.handshake()
if(y == True):
	print("fake server handshake complete!\n")
	mim_server.send_to_client("Welcome to the server. Type something and hit enter\n")
else:
	print('Fake Server handshake failed\n')


if(x == True and y == True):
	while True:
		client_message = mim_server.recv_from_client()
		mim_client.send_to_server(client_message)
		server_message = mim_client.recv_from_server()
		mim_server.send_to_client(server_message)
