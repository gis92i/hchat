import socket 
import sys
from Crypto.Util.number import getRandomRange
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util.number import inverse
from Crypto.Util.number import long_to_bytes
from Crypto.Util.number import bytes_to_long
from Crypto.Hash import SHA256


class Client:
	""" Client class! p (big prime) and  generator g are shared accross all instances of the Client class
	    The values of p and g were taken from the website https://www.ietf.org/rfc/rfc3526.txt
	"""

	p = int("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF",16)
	g = 2


	def __init__(self,host_ip,host_port):
		try:
			#Create an AF_INET, STEAM socket (TCP)
			self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except socket.error, msg:
			print 'Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1]
			sys.exit()

		print 'Socket Created'

		self.server_host = host_ip
		self.server_port = host_port

		self.b = getRandomRange(1,self.p)
		self.g_b = pow(self.g,self.b,self.p)
		self.diff = 0
		self.aes_key = ''
		self.iv = 16 * '\x00' #iv is constant so that we can decrypt and encrypt with the server and client having the same iv
		self.commitedvalue = 0 #This variable will be used to check the commitment in order to avoid the man in the middle


		"""
		We start the secret channel talk right here for the man in the middle scheme
		"""
		self.secret_channel_talk()

	def connect(self):
		print "Connecting to:" + str(self.server_host) + " port: " + str(self.server_port)
		self.s.connect((self.server_host,self.server_port))
		print "Connected!"

	def send_to_server(self,string):
		#When we send to server we first change the message to an encrypted value 
		#Then we encrypt it using AES with self.key
		cipher = AES.new(self.aes_key,AES.MODE_CFB, self.iv)
		msg = cipher.encrypt(string)
		self.s.sendall(msg)

	def recv_from_server(self):
		#When we receive we decrypt with AES and the key 
		msg = self.s.recv(1024)
		#Now we decrypt the message which we then return
		cipher = AES.new(self.aes_key,AES.MODE_CFB, self.iv)
		message = cipher.decrypt(msg)
		return message
    

	def handshake(self):
		#we will create an aes key here with the password being gab
		#Receives the authentication welcome message
		reply = self.s.recv(1024)
		print reply

		#receive the diffie stuff
		g_a = int(self.s.recv(1024))
		#send the diffie stuff 
		self.s.sendall(str(self.g_b))

		"""
		Before we continue we verify the value we received through the secret channel
		"""
		fhash = SHA256.new()
		fhash.update(long_to_bytes(g_a))
		if(self.commitedvalue != fhash.hexdigest()):
			print("This server is a fake server! Closing the connetions")
			#self.quit()
			return False

		self.diff = pow(g_a,self.b,self.p)
		temp = long_to_bytes(self.diff) #we set the key here 
		h = SHA256.new()
		h.digest_size = 32
		h.update(temp)
		self.aes_key = h.digest()
		return True

		print(self.diff)

	def talk_to_server(self):
		welcome = self.recv_from_server()
		print(welcome)

		while True:
	
			message = raw_input("")
			try:
				self.send_to_server(message)
			except socket.error:
				print "Send failed"
				sys.exit()

			reply = self.recv_from_server()
			print reply

	def secret_channel_talk(self,host1='localhost',port1=8888):
		"""
		This function will be used to stimulate a secret channel aka a phone call for the commitment
		NOTE: I gave it the values localhost and port 8888 because in this project scheme
		localhost 8888 is always the valid server and localhost 6666 is the devil server (MIM)
		"""
		print "Secret Channel Connecting to:" + str(host1) + " port: " + str(port1)
		try:
			#Create an AF_INET, STEAM socket (TCP)
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		except socket.error, msg:
			print 'Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1]
			sys.exit()

		print 'Socket Created'

		sock.connect((host1,port1))
		print "Connected!"
		self.commitedvalue = sock.recv(1024)
		h = SHA256.new()
		h.update(long_to_bytes(self.g_b))
		sock.sendall(h.hexdigest()) #Sending the Client's commited value 
		sock.close()
		print("Commitment process done!")


	def quit(self):
		self.s.close()


if __name__ == '__main__':

	host = sys.argv[1]
	try:
		port = int(sys.argv[2])
	except ValueError:
		print "Not a number"

	client = Client(host,port)
	#client.secret_channel_talk()
	client.connect()
	client.handshake()
	client.talk_to_server()
	client.quit()







